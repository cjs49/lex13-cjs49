# lex13-cjs49

Basic idea:
We define a function which constructs a new date representing the day after a struct is input representing today. The function then returns a ptr to  the next day struct.   
    The code should be able to handle all dates from Jan 1st of 1582, to Dec 31, 2500.


We also have to account for leap years. A formula is provided

"if (year is not divisible by 4) then (it is a common year)
else if (year is not divisible by 100) then (it is a leap year)
else if (year is not divisible by 400) then (it is a common year)
else (it is a leap year) "

Code:
We start by creating an actual node, as listed in the questionaire form.
Struct Date{
int date;
int month;
int year;

}

 We also create a pointer that will act as the head of the list, and will be
used to traverse said list.

This pointer is also declared in the lex13 questionaire
struct Date * nextDay (struct Date * today); 

Where nextDay is regarded as the same pointer type as struct Date * <thing> .

We can use it as also stated in lex13

struct Date * today;
today = (struct Date * ) malloc(sizeof(today));
( * today ).date = 14
( * today).month = 3
( * today ).year = 2019 



