#include <stdio.h>
#include <stdbool.h>
#include "date.h"
#include <stdlib.h>

int main()
{
//Test to see if we are actually returning null from date.h


struct Date *testPtr;

testPtr = (struct Date *) malloc(sizeof(testPtr));
(*testPtr).date = 14;
(*testPtr).month = 3;
(*testPtr).year = 2019; 


if( testPtr == NULL)
{
 printf("Our original ptr has been set to NULL.\n\n");
}
else
{
printf("Our original ptr has been set to something other than NULL\n\n");
}



if ( (testPtr =  nextDayFunc(testPtr)) == NULL )
{
printf("Our function is returning null.\n");

}
else
{

printf("Our function is not returning null.\n");
printf("The value of our next day date is: %d\n", testPtr->date);



}
 
return 0;


}

