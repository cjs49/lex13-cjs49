#Code for makefile, including paths for CUNIT, are taken from lex08. I do not yet understand how to incorporate CUNIT into makefiles.   


ifeq ($(OS), Darwin)
  CUNIT_PATH_PREFIX = /usr/local/Cellar/cunit/2.1-3/
  CUNIT_DIRECTORY = cunit
endif
ifeq ($(OS), Linux)
  CUNIT_PATH_PREFIX = /util/CUnit/
  CUNIT_DIRECTORY = CUnit/
endif



CC = gcc
CFLAGS = -g -Wall -std=c11 
Objects = main.c date.o

runner: main.c date.o 
	$(CC) $(CFLAGS) -o runner date.o main.c


date.o: date.c date.h


test.o: test.c

tests: date.o test.o
	$(CC) $(CFLAGS) -o tests date.o test.c







clean:
	rm -f *.o runner tests
